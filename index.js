const express = require("express")
const buildUrl = require('build-url')
const request = require('request')
const users = require('./routes/users')
const suggest = require('./routes/suggest')
const reviews = require('./routes/reviews')
const book = require('./routes/book')
const cors = require('cors')
const app = express()

app.use(cors())

app.use('/users', users)

app.use('/suggest', suggest)

app.use('/reviews', reviews)

app.use('/static', express.static('public'))

app.get('/seats', function(req, res) {
	res.sendFile(__dirname + '/public/seats.html')
})

app.use('/book',book)

app.get('/search',function(req,res) {

	console.log(req.query)
	let searchParams = {
		classType: req.query.classType,
		dcID: "",
		deptDate: req.query.deptDate,
		filterReq: "",
		fromCity: req.query.fromCity,
		isDateChange: "",
		lob:"Flight",
		noOfAdlts:1,
		noOfChd:0,
		noOfInfnt:0,
		//returnDate:"23/11/2017",
		toCity: req.query.toCity,
		tripType:"O",
		tripTypeDup:"O"
	}

	var searchurl = buildUrl('http://flights.makemytrip.com', {
		path: '/makemytrip/search-api.json',
		queryParams: searchParams
	})

	console.log(searchurl)
		
	
	request({
		url: searchurl,
		headers: {
			'User-Agent': 'request'
		}
	}, function (error, response, body) {
//		console.log(JSON.parse(response.body))
	  	return res.json(JSON.parse(response.body))
	});
	
})

/*
*	App is listening at port 3000
*/
app.listen(5000, function(err) {
	if(err) throw err
	console.log('App listening on port 5000!')
})