-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2017 at 04:53 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `airlinedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookticket`
--

CREATE TABLE `bookticket` (
  `TicketID` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `airplaneName` varchar(100) NOT NULL,
  `FromCity` varchar(100) NOT NULL,
  `ToCity` varchar(100) NOT NULL,
  `passenger` varchar(100) NOT NULL,
  `Age` int(11) NOT NULL,
  `DOB` date NOT NULL,
  `Gender` varchar(100) NOT NULL,
  `passport` varchar(100) NOT NULL,
  `visa` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bookticket`
--

INSERT INTO `bookticket` (`TicketID`, `email`, `date`, `airplaneName`, `FromCity`, `ToCity`, `passenger`, `Age`, `DOB`, `Gender`, `passport`, `visa`) VALUES
(1, 'katwaabhishek@gmail.com', '0000-00-00', 'xyz', 'Bang', 'USA', '', 0, '0000-00-00', '', '', ''),
(2, 'katwaabhishek', '2017-08-01', 'xyz', 'Ci', 'Bi', '', 0, '0000-00-00', '', '', ''),
(3, 'abhishek', '2000-10-10', 'XYZ', 'fromCity', 'ToCity', '', 0, '0000-00-00', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookticket`
--
ALTER TABLE `bookticket`
  ADD PRIMARY KEY (`TicketID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookticket`
--
ALTER TABLE `bookticket`
  MODIFY `TicketID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
