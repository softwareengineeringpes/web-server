# FlyNow

An airline ticket and seat registeration system.

### Version
1.0.0

## Prerequisites

* [NodeJS](https://nodejs.org/)
* [ExpressJS](http://expressjs.com/)

## Installation

Clone this repository

```bash
git clone https://bitbucket.org/softwareengineeringpes/web-server.git
```

Change into the directory

```bash
cd web-server
```

Install the dependencies

```bash
npm install
```

### Running

Run the app

```bash
node index
```

## Built With

* Serverside - [NodeJS](https://nodejs.org/), [ExpressJS](http://expressjs.com/)
* Database - [MySQL](https://www.mysql.com/)

## Authors

* [**Ahmed Fouzan**](https://github.com/ahmedfouzan)
* [**Abhishek Katwa**](https://www.linkedin.com/in/abhishek-katwa-6738a1129/)
* More authors to be added

## License

[MIT](http://opensource.org/licenses/MIT)