const express = require('express')
const router = express.Router()
const request = require('request')
const con = require('../config/database')
/*
*	Whenever user queries for localhost:3000/book
*	This gets executed, where booking of the ticket takes place
*	Query Parameters : email of registered member,
*					   date on which this request is made
*					   airplane name,FromCity,toCity,
*					   Passenger Details : Name,Age,DOB,Gender,passport,visa
*	Result : This is stored in the Database and a ticketNumber will be generated and is stored in the database
*/
router.get('/', function(req, res) {
    try { 
    	
        let email = req.query.email //email of registered member
        let date = req.query.date
        let airplaneName = req.query.airplaneName
        let fromCity = req.query.fromCity
        let toCity = rew.query.toCity

        let passenger = req.query.passenger
        let Age =req.query.age
        let dob = req.query.dob
        let Gender = req.query.Gender
        let passport = req.query.passport
        let visa = req.query.visa

        //having email,date,airplaneName , From and To Cities
        //Date in the format xxxx-xx-xx

        var sql="INSERT INTO BookTicket( email, date, airplaneName, FromCity, ToCity, passenger, Age, DOB, Gender, passport, visa) VALUES ('"+ email +"','"+ date +"','"+ airplaneName +"','"+ fromCity +"','"+ toCity +"','"+ passenger +"','"+ Age +"','"+ dob +"','"+ Gender +"','"+ passport +"','"+ visa +"');";
        con.query(sql, function (err, result) {
        if (err) throw err;
        res.json({success: true, data: result})
        });

    } catch (error) {
        res.json({success:false, message:"Something went wrong!!"})
    }
})



/*
*	If we want to get to know all the Booking the registered member Made,
*	Query : localhost:3000/book/history
*	QueryParameter : email
*	Logic : Search for all the booking made by this registered user, and return the Details of those booking in JSON format
*	If some error, Then it will return Something went wrong
*/
router.get('/history', function(req, res) {
    try { 
        let email = req.query.email
        //returning all the flights along with the ticketID and the names of Flights and From - To City

        var sql="SELECT * FROM BookTicket WHERE email='"+ email +"';";
        con.query(sql, function (err, result) {
        if (err) throw err;
        res.json(result)
        });

    } catch (error) {
        res.json({success:false, message:"Something went wrong!!"})
    }
})

/*
*	If the user wants to query for details of ticketNumber
*	Query localhost:3000/book/ticket
* 	QueryParameter : ticketNumber
*	Result : Details of the booking with that TicketNumber 
*/
router.get('/ticket', function(req, res) {
    try { 
        let ticketID = req.query.ticketID
        //returning all the flights along with the ticketID and the names of Flights and From - To City

        var sql="SELECT * FROM BookTicket WHERE TicketID='"+ ticketID +"';";
        con.query(sql, function (err, result) {
        if (err) throw err;
        res.json(result)
        });

    } catch (error) {
        res.json({success:false, message:"Something went wrong!!"})
    }
})

module.exports = router