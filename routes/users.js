const express = require('express')
const router = express.Router()
const con = require('../config/database')

// Get all users

router.get('/', function(req, res) {
    try {

        // Get the data from database and send an array of JSON Objects
        var sql="SELECT * FROM users";
        con.query(sql, function (err, result) {
        if (err) throw err;
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(result));
        });

        //res.json()
    } catch (error) {
        res.status(500).send('Something went wrong')
    }
})

// Get a user by userId
/*
*   If we want to get to know details of registered member,
*   Query : localhost:3000/users/:ID
*   Input : ID is the input in URI
*   Logic : Search for the registered user who has UserID as Input, and return the Details of User in JSON format
*   If some error, Then it will return Something went wrong
*/

router.get('/:userId([1-9])', function(req, res) {
    try {
        let userId = req.params.userId
        // Get the data from database and a JSON of user data
        var sql="SELECT * FROM users where UserID ='"+userId+"';";
        con.query(sql, function (err, result) {
        if (err) throw err;
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(result));
        });

        //res.json()
    } catch (error) {
        res.status(500).send('Something went wrong')
    }
})


// Authenticate a user by userId
/*
*   Authentication
*   Query : localhost:3000/users/authenticate
*   QueryParameter : email, password
*   Logic : If the QueryParameters match to data in Database "users" , return success:true,
*   Otherwise return success : false
*   If some error, Then it will return Something went wrong
*/
router.get('/authenticate', function(req, res) {
    try {

        let email = req.query.email
        let password = req.query.password
        
        // Authenticate the user with the userId by getting password and username from query parameters
        
        var sql="SELECT * FROM users where Email ='" + email + "';"

        con.query(sql, function (err, result) {
            if (err) throw err
            if(password == result[0].Password){
                res.json({
                    success: true,
                    message: "User authenticated",
                    userId: result[0].UserID
                })
            } else {
                res.json({
                    success: false,
                    message: "User could not authenticated"
                })
            }
        })

        //res.json()
    } catch (error) {
        res.status(500).send('Something went wrong')
    }
})


// Create a new user
/*
*   Registration of members(New User)
*   Query : localhost:3000/users/create
*   QueryParameter : email, mobileNumber, password
*   Logic : Cretae a new user with the details in QueryParamters in users Table
*   On succes return success: true.
*   If some error, Then it will return Something went wrong
*/
router.get('/create', function(req, res) {
    try {
        
        // Create a new user from the json data in the req.body
        let email = req.query.email
        let mobileNum = req.query.mobile
        let password = req.query.password
    
        var sql="INSERT INTO users(Email, Mobile, Password) VALUES ('"+ email + "','" + mobileNum +"','"+ password +"');";
        
        con.query(sql, function (err, result) {
            if (err) throw err;
            res.json({
                success: true,
                message: 'User created'
            });
        });
    } catch (error) {
        console.log(error)
        res.status(500).send('Something went wrong')
    }
})

// Modify an existing user data
/*
*   Registration of members(New User),Adding all other details of User
*   Query : localhost:3000/users/update/:UserID
*   QueryParameter : address, DOB, Gender, passport, visa
*   Logic : Update the user with the details in QueryParamters in users Table
*   On succes return success: true.
*   If some error, Then it will return Something went wrong
*/
router.get('/update/:userId', function(req, res) {
    try {
        
        let userId = req.params.userId

        // Modify an existing user from the json data in the req.body
        let address = req.query.address
        let dob = req.query.dob
        let Gender = req.query.Gender
        let passport = req.query.passport
        let visa = req.query.visa


        var sql="UPDATE users SET address='"+ address +"',dob='"+ dob +"',Gender='"+ Gender +"',PassportNumber='"+ 
       passport +"',VisaCard='"+ visa +"' WHERE UserID='"+ userId +"';";
        
        con.query(sql, function (err, result) {
        if (err) throw err;
        res.setHeader('Content-Type', 'application/json');
        res.send({
                success: true,
                message: 'User Updated'
            });
        });

        //res.json()
    } catch (error) {
        res.status(500).send('Something went wrong')
    }
})


// Delete user by userId
/*
*   Delete User
*   Query : localhost:3000/users/delete/:UserID
*   Input : UserID
*   Logic : Remove the user form users Table with that UserID
*   On succes return success: true.
*   If some error, Then it will return Something went wrong
*/
router.get('/delete/:userId', function(req, res) {
    try {
        
        let userId = req.params.userId

        // Delete user and sent success message
        var sql="DELETE FROM users WHERE UserID = '"+ userId+"';";
        con.query(sql, function (err, result) {
        if (err) throw err;
        res.setHeader('Content-Type', 'application/json');
        res.send({
                success: true,
                message: 'User Destroyed'
            });
        });


        //res.json()
    } catch (error) {
        res.status(500).send('Something went wrong')
    }
})

module.exports = router