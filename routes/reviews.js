const express = require('express')
const router = express.Router()
const con = require('../config/database')

router.get('/:count', function(req, res) {
    let count = req.params.count
    // Limit the results to the count
    try {
        
        var sql="SELECT * FROM reviews LIMIT " + count;
        con.query(sql, function (err, result) {
            if (err) throw err;
            return res.json(result);
        });

    } catch (error) {
        return res.status(500).send('Something went wrong')
    }
})

module.exports = router