const express = require('express')
const router = express.Router()
const con = require('../config/database')

/*
*	Whenever user queries for localhost:3000/suggest/cities
*	This gets executed, where we match the cities with that startsWith the usersInput,
*	 so that the user gets notified with all the cities with that current input substring
*	Query Parameters : startsWIth (String) , part of cityname
*	Result : All cities which starts with the current input string.
*/
router.get('/cities', function(req, res) {
    try { 
        let startsWith = req.query.startsWith

        let cities = []
        
        //get cities that starts with the query parameter 'startsWith'
        var sql="SELECT AirportID,Name,City,Country,IATA FROM airport where City like '"+ startsWith +"%' ORDER BY City";
        con.query(sql, function (err, result) {
            if (err) throw err;

        // Result should be sorted alphabetically

        // each city will be in a json object along with the IATA code
        /** Eg : {
         *          cityName: 'Bangalore',
         *          IATA: 'BLR'
         *       }
         * 
         */
         //console.log(result);
            res.json({success: true, data: result})
        });
    } catch (error) {
        res.json({success:false, message:"Something went wrong!!"})
    }
})

module.exports = router